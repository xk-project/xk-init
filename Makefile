all: init

include Makefile.in

_LINKER=lld
LD = ld.$(_LINKER)
LDFLAGS = -fuse-ld=$(_LINKER) -static-libgcc $(MUSLFLAGS) -L-user-start -L/usr/lib/musl/lib -L-user-end -static

BLOBS  = core
BLOBS += contrib

rndmac = $(shell echo -n "52:54:00" && hexdump -n3 -e '/1 ":%02x"' /dev/urandom)

run: all bzImage
	qemu-system-x86_64 \
		-name xk \
		-display none \
		-serial mon:stdio \
		-nodefaults \
		-machine q35,accel=kvm -cpu max \
		-smp cores=2,threads=1,sockets=1 \
		-m 128 \
		-drive if=pflash,format=raw,readonly,file="/usr/share/ovmf/x64/OVMF_CODE.fd" \
		-device virtio-rng \
		-device virtio-keyboard \
		-netdev user,id=net0,hostfwd=tcp::29292-:29292 -device virtio-net,mac=$(call rndmac),netdev=net0,mq=on,vectors=6 \
		-fsdev local,id=fs0,path=root,security_model=none,writeout=immediate \
		-device virtio-9p-pci,fsdev=fs0,mount_tag=/dev/root \
		-kernel bzImage \
		-append "init=/bin/init threadirqs console=ttyS0,115200 loglevel=7 root=/dev/root rw rootfstype=9p rootflags=trans=virtio,version=9p2000.L ip=dhcp"

init: $(BLOBS)
	$(CC) $(LDFLAGS) $(^:=/builtin.a) -o $@

$(BLOBS):
	$(MAKE) -C $@

clean:
	for blob in $(BLOBS); do $(MAKE) -C $$blob clean; done
	rm -f init

.PHONY: $(BLOBS)
