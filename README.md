xk
==

Description
-----------

A container-like macrokernel environment based on Linux and KVM.

Building
--------

### Prerequisites

* `Linux kernel` (tested with v5.9)
* `clang` (tested with v10.0.1)
* `lld` (tested with v10.0.1)
* `musl` (tested with v1.2.1)
* `make` (tested with v4.3)

### Compiling

Just type `make`.

Distribution and Contribution
-----------------------------

Distributed under terms and conditions of GNU GPL v3 (only).

Bundled `libconfig` is distributed under terms and conditions of GNU LGPL v2.1.

Developers:

* Oleksandr Natalenko &lt;oleksandr@natalenko.name&gt;
