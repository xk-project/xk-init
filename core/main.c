#include <asm-generic/setup.h>
#include <errno.h>
#include <linux/reboot.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/reboot.h>
#include <sys/wait.h>
#include <sysexits.h>
#include <unistd.h>

#include "../contrib/libconfig/libconfig.h"
#include "xk_err.h"
#include "xk_fs.h"
#include "xk_net.h"

#define XK_CONF	"/etc/xk.conf"
#define XK_NIC	"eth0"

struct xk_config
{
	size_t argc;
	const char** argv;
	size_t envc;
	const char** envp;
	bool restart;
};

static bool do_mounts(void)
{
	if (!xk_mount("/proc", "proc"))
		goto err;

	if (!xk_mount("/sys", "sysfs"))
		goto err;

	if (!xk_mount("/dev", "devtmpfs"))
		goto err;

	if (!xk_mkdir("/etc"))
		goto err;

	if (!xk_mkdir("/tmp"))
		goto err;

	if (!xk_mkdir("/root"))
		goto err;

	return true;

err:
	return false;
}

static bool do_network(struct xk_config* const _config)
{
	if (!xk_net_set_link_queues(XK_NIC))
		goto err;

	if (!xk_net_add_ns())
		goto err;

	return true;

err:
	return false;
}

static bool do_cmdline(struct xk_config* const _config)
{
	config_t cfg;
	config_setting_t* setting = NULL;
	config_setting_t* cur = NULL;

	memset(&cfg, 0, sizeof cfg);

	config_init(&cfg);
	if (config_read_file(&cfg, XK_CONF) == CONFIG_FALSE)
		goto err;

	/* run */
	setting = config_lookup(&cfg, "argv");
	if (!setting)
		goto err;

	_config->argc = config_setting_length(setting);
	_config->argv = calloc(_config->argc + 1, sizeof(char*));
	for (size_t i = 0; i < _config->argc; i++)
	{
		cur = config_setting_get_elem(setting, i);
		_config->argv[i] = config_setting_get_string(cur);
	}
	_config->argv[_config->argc] = NULL;

	setting = config_lookup(&cfg, "envp");
	if (!setting)
	{
		_config->envc = 1;
		_config->envp = calloc(1, sizeof(char*));
		_config->envp[0] = NULL;
		goto no_envp;
	}

	_config->envc = config_setting_length(setting);
	_config->envp = calloc(_config->envc + 1, sizeof(char*));
	for (size_t i = 0; i < _config->envc; i++)
	{
		cur = config_setting_get_elem(setting, i);
		_config->envp[i] = config_setting_get_string(cur);
	}
	_config->envp[_config->envc] = NULL;

no_envp:
	setting = config_lookup(&cfg, "restart");
	if (!setting)
	{
		_config->restart = false;
		goto no_restart;
	}

	_config->restart = config_setting_get_bool(setting);

no_restart:
	return true;

err:
	return false;
}

static bool do_exec(struct xk_config* const _config)
{
	pid_t pid = -1;
	int status = -1;

	while (true)
	{
		pid = fork();
		if (pid == 0)
		{
			if (execve(_config->argv[0], _config->argv, _config->envp) == -1)
			{
				pr_err("execve", "Daemon: %s", _config->argv[0]);
				exit(EX_SOFTWARE);
			}
		} else if (pid > 0)
		{
			if (waitpid(pid, &status, 0) == -1)
			{
				pr_err("waitpid", "Daemon: %s", _config->argv[0]);
				goto err;
			} else if (WIFEXITED(status) && WEXITSTATUS(status) != 0)
			{
				errno = 0;
				pr_err("waitpid", "Child exit status: %d\n", WEXITSTATUS(status));
				goto out;
			}
		} else
		{
			pr_err("fork", "Daemon: %s", _config->argv[0]);
			goto err;
		}

		if (!_config->restart)
			break;
	}

out:
	return true;

err:
	return false;
}

int main(int _argc, char** _argv)
{
	(void)_argc;
	(void)_argv;

	sigset_t mask;
	struct xk_config config;

	sigfillset(&mask);
	sigprocmask(SIG_SETMASK, &mask, NULL);
	memset(&config, 0, sizeof config);

	if (!do_mounts())
	{
		pr_err("init", "%s", "Unable to set up mountpoints");
		goto err;
	}

	if (!do_cmdline(&config))
	{
		pr_err("init", "%s", "Unable to parse config file");
		goto err;
	}

	if (!do_network(&config))
	{
		pr_err("init", "%s", "Unable to set up networking");
		goto err;
	}

	if (!do_exec(&config))
	{
		pr_err("init", "Unable to execute \"%s\"", config.argv[0]);
		goto err;
	}

err:
	if (reboot(LINUX_REBOOT_CMD_POWER_OFF) == -1)
	{
		pr_err("reboot", "%s", "Unable to call a power off");
		goto out;
	}

	pr_err("reboot", "%s", "Unable to perform a power off");

out:
	return 0;
}

