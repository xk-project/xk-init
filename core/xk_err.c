#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "xk_err.h"

void __pr_err(const char* _rt, const char* _func, int _line, const char* _fmt, ...)
{
	va_list args;

	va_start(args, _fmt);

	fprintf(stderr, "[%s, %s:%d]: ", _rt, _func, _line);
	vfprintf(stderr, _fmt, args);
	fprintf(stderr, "%s", "\n");
	fprintf(stderr, "[%s, %s:%d]: %s\n", _rt, _func, _line, strerror(errno));

	va_end(args);

	return;
}

