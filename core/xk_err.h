#pragma once

#include <stdarg.h>
#include <stdio.h>

#define pr_err(A, B, ...)	__pr_err(A, __func__, __LINE__, B, __VA_ARGS__)

__attribute__((format(printf, 4, 5)))
void __pr_err(const char*, const char*, int, const char*, ...);

