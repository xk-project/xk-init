#include <errno.h>
#include <sys/mount.h>
#include <sys/stat.h>

#include "xk_err.h"

#include "xk_fs.h"

bool xk_mkdir(const char* _target)
{
	if (mkdir(_target, 0755) == -1 && errno != EEXIST)
	{
		pr_err("mkdir", "Folder: %s", _target);
		goto err;
	}

	return true;

err:
	return false;
}

bool xk_mount(const char* _target, const char* _type)
{
	if (!xk_mkdir(_target))
		goto err;

	if (mount(NULL, _target, _type, 0, NULL) == -1)
	{
		pr_err("mount", "Mountpoint: %s, type: %s", _target, _type);
		goto err;
	}

	return true;

err:
	return false;
}

