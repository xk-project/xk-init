#pragma once

#include <stdbool.h>

bool xk_mkdir(const char* _target);
bool xk_mount(const char* _target, const char* _type);

