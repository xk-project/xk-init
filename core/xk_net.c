#include <arpa/inet.h>
#include <linux/ethtool.h>
#include <linux/sockios.h>
#include <net/if.h>
#include <net/route.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <unistd.h>

#include "xk_err.h"

#include "xk_net.h"

#define XK_NET_PNP		"/proc/net/pnp"
#define XK_RESOLV_CONF	"/etc/resolv.conf"

bool xk_net_set_link_queues(const char* _if)
{
	int fd = -1;
	unsigned int max_combined = 0;
	unsigned int queues = 0;
	struct ifreq req;
	struct ethtool_channels cmd;

	memset(&req, 0, sizeof req);
	memset(&cmd, 0, sizeof cmd);

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd == -1)
	{
		pr_err("socket", "Interface: %s", _if);
		goto err;
	}

	cmd.cmd = ETHTOOL_GCHANNELS;
	strncpy(req.ifr_name, _if, IFNAMSIZ);
	req.ifr_data = (void*)&cmd;
	if (ioctl(fd, SIOCETHTOOL, &req) == -1)
	{
		pr_err("ioctl", "Interface: %s", _if);
		goto err;
	}

	max_combined = cmd.max_combined;
	queues = sysconf(_SC_NPROCESSORS_ONLN);

	memset(&req, 0, sizeof req);
	memset(&cmd, 0, sizeof cmd);
	cmd.cmd = ETHTOOL_SCHANNELS;
	cmd.combined_count = MIN(queues, max_combined);
	strncpy(req.ifr_name, _if, IFNAMSIZ);
	req.ifr_data = (void*)&cmd;
	if (ioctl(fd, SIOCETHTOOL, &req) == -1)
	{
		pr_err("ioctl", "Interface: %s", _if);
		goto err;
	}

	if (close(fd) == -1)
	{
		pr_err("close", "Interface: %s", _if);
		goto err;
	}

	return true;

err:
	return false;
}

bool xk_net_add_ns(void)
{
	FILE* fin = NULL;
	FILE* fout = NULL;
	ssize_t read = 0;
	size_t size = 0;
	char* line = NULL;

	fin = fopen(XK_NET_PNP, "r");
	if (!fin)
		goto err;

	fout = fopen(XK_RESOLV_CONF, "w");
	if (!fout)
		goto err;

	while ((read = getline(&line, &size, fin)) != -1)
	{
		if (!strncmp(line, "nameserver ", 11))
			fprintf(fout, "%s\n", line);
		free(line);
		line = NULL;
		size = 0;
	}

	if (fclose(fout) == EOF)
		goto err;

	if (fclose(fin) == EOF)
		goto err;

	return true;

err:
	return false;
}

